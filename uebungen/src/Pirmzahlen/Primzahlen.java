package Pirmzahlen;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Primzahlen {
    static int[] ersteHundert = new int[100];
    static ArrayList<Integer> primfaktoren = new ArrayList<Integer>();
    static int userZahl;
    int counter = 0;
    public static void main(String[] args) {
       Primzahlen hallo = new Primzahlen();
       hallo.zerlegung();
       System.out.println(primfaktoren);
    }

    private void berechnen() {
        int zahl = 0;
        int j= 0;
        while (counter < 100) {
            zahl++;
            if (checkPrim(zahl)) {
                counter ++;
            //    System.out.println(zahl);
                ersteHundert[j]=zahl;
                j++;
            }
        }
    }
    public boolean checkPrim(int zahl) {
        for (int i = 2; i < zahl; i++) {
            if (zahl % i == 0) {
                return false;
            }
        }
        return true;
    }
    public void zerlegung() {
        int faktor = 2;
        System.out.println("Geben sie eine Zahl ein");
        Scanner scanner = new Scanner(System.in);
        int userZahl = scanner.nextInt();
        while (checkPrim(userZahl) == false){
            if (userZahl % faktor == 0){
                primfaktoren.add(faktor);
                userZahl = userZahl / faktor;
            }else{
                faktor = getNextPrimzahl(faktor);
            }
        }
        primfaktoren.add(userZahl);
    }
    public int getNextPrimzahl(int faktor){
        faktor++;
        while (checkPrim(faktor) == false){
            faktor ++;
        }
        return faktor;
    }
}
