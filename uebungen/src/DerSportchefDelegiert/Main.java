package DerSportchefDelegiert;

public class Main {
    public static void main(String[] args){
        new Main().erschaffen();
    }
    void erschaffen(){
        Trainer trainer = new Trainer();
        Sportchef chef = new Sportchef();
        chef.trainer = trainer;
        playersAdd(trainer);
        chef.trainieren();
    }
    void playersAdd(Trainer trainer){
        playerAdd(trainer, "Michi ");
        playerAdd(trainer, "Wunderli ");
        playerAdd(trainer, "Ronaldo ");
        playerAdd(trainer, "Messi ");
        playerAdd(trainer, "Ronaldinho ");
        playerAdd(trainer, "Samen Schwanzer ");
        playerAdd(trainer, "Florian Genge ");
        playerAdd(trainer, "Fabio Nyffe ");
        playerAdd(trainer, "Gugus ");
        playerAdd(trainer, "Hans-Peter ");
        playerAdd(trainer, "Ruedi ");
    }
    void playerAdd(Trainer trainer, String rufName){
        Spieler s;
        s = new Spieler();
        s.rufName = rufName;
        trainer.spielerHinzufüegen(s);
    }
}
