package Quader2;

public class QuaderMain {
    static double volume;
    static double fläche;
    static Quader2 quader = new Quader2(3,5,7);
    public static void main(String[] args) {
        volumenBerechnen();
        flächeberechnen();
    }

    public double getVolume() {
        return volume;
    }
    public void setVolume(double volume) {
        this.volume = volume;
    }
    public static void volumenBerechnen(){
        volume = quader.getL() * quader.getB() * quader.getH();
        System.out.println(volume);
    }
    public static void flächeberechnen() {
        fläche = 2* quader.getL()* quader.getB()+2* quader.getL()* quader.getH()+2* quader.getB()* quader.getH();
        System.out.println(fläche);

    }
}

