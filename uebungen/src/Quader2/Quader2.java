package Quader2;

public class Quader2 {
    public int Quader;
    private int l;
    private int b;
    private int h;

    public int getL() {
        return l;
    }

    public int getB() {
        return b;
    }

    public int getH() {
        return h;
    }

    public void setL(int l) {
        this.l = l;
    }
    public void setB(int b) {
        this.b = b;
    }
    public void setH(int h) {
        this.h = h;
    }
    public Quader2(int l, int b, int h) {
        this.l = l;
        this.b = b;
        this.h = h;
    }
}
