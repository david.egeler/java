package socialnetworking;
import java.util.ArrayList;

public class MessagePost extends Post {
    private String message;

    public MessagePost(String username, String message) {
        super(username);
        this.message = message;
    }

    public String getText() {
        return message;
    }

    /**
     * Displays details of post
     */
    public void display() {
        System.out.println("Message " + message);
        super.display();
    }
}
