package socialnetworking;

import java.util.ArrayList;
import java.util.Date;

public class EventPost {
    private String username;

    private long timeStamp;

    private String description;

    private Date eventDate;

    private int likes;

    private ArrayList<String> Kommentare;

    public EventPost(String username, long timeStamp, String description, Date eventDate, int likes, ArrayList<String> kommentare) {
        this.username = username;
        this.timeStamp = timeStamp;
        this.description = description;
        this.eventDate = eventDate;
        this.likes = likes;
        Kommentare = kommentare;
    }
}
