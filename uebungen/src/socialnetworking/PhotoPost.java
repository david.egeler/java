package socialnetworking;
import java.util.ArrayList;

public class PhotoPost extends Post {
    private String filename;
    private String caption;

    public PhotoPost(String username, String filename, String caption) {
        super(username);
        this.filename = filename;
        this.caption = caption;
    }

    public String getImageFile() {
        return filename;
    }

    public String getCaption() {
        return caption;
    }

    /**
     * Displays details of post
     */
    public void display() {
        System.out.println("FileName " + "{" + filename + "}");
        System.out.println("Caption " + caption);
        super.display();
    }
}