package ameise;

public class Spielfeld {
    public static char [][] feld;
    public char target = '*';
    public Ameise ameise = new Ameise();
    
    public Spielfeld() {
        this.feld = new char[100][100];
        ameise.y = 50;
        ameise.x = 50;
        ameise.r = "l";
        

    }
    public void fuellen(){
        for (int j = 0; j < 100; j++) {

            for (int i = 0; i < 100; i++) {
                feld[j][i] = ' ';
            }
        }
    }

    public void feldzeigen(){
        for (int j = 0; j < feld.length; j++){
            String printString = "";
            for (int i = 0; i < feld[0].length; i++){
                printString += feld[j][i] + " ";
            }
            System.out.println(printString);
        }
    }
    public void faerben(){
        while(ameise.x<100 && ameise.y<100 && ameise.x>0 && ameise.y>0) {
            if (Character.compare(feld[ameise.x][ameise.y], '*') == 0) {
                feld[ameise.x][ameise.y] = ' ';
                ameise.drehrechts();
            } else {
                feld[ameise.x][ameise.y] = '*';
                ameise.drehlinks();
            }
            nextField();
        }
    }
    public void nextField() {
        switch (ameise.r){
            case "l":
                ameise.y--;
                break;
            case "r":
                ameise.y++;
                break;
            case "u":
                ameise.x++;
                break;
            case "o":
                ameise.x--;
                break;
            default:
                break;
        }
    }
    public void run(){
        fuellen();
        faerben();
        feldzeigen();
    }

}
