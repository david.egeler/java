package ameise;

public class Ameise {
    public int Ameise;
    public int x;
    public int y;
    public String r;
    public void drehlinks() {
        switch(r){
            case "l":
                r = "u";
                break;
            case "r":
                r = "o";
                break;
            case "o":
                r = "l";
                break;
            case "u":
                r = "r";
                break;
            default:
                break;
        }
    }
    public void drehrechts() {
        switch(r){
            case "l":
                r = "o";
                break;
            case "r":
                r = "u";
                break;
            case "o":
                r = "r";
                break;
            case "u":
                r = "l";
                break;
            default:
                break;
        }
    }
}

