package Minesweeper;
import java.util.Scanner;

public class GameRunner{


    public static void init(){
        System.out.println("Das Ziel des Spiels ist es, das Feld von allen sicheren Kacheln zu säubern.");
        System.out.println("Spielen Sie, indem Sie die Koordinaten einer Kachel eingeben, die Sie auswählen möchten.");
        System.out.println("Die Reichweite der Kacheln ist 1-10. Der Ursprung ist die obere linke Kachel.");
        System.out.println("Es gibt 16 Minen. Das Auswählen eines Felds mit einer Mine beendet das Spiel.");
    }

    public static void test(){
        Game game = new Game();
        game.generateMines(16);
        game.update();
        Scanner scan = new Scanner(System.in);

        int x, y;
        System.out.print("X-Koordinate Eingeben:");
        x = scan.nextInt();
        System.out.print("Y-Koordinate Eingeben:");
        y = scan.nextInt();


        if(game.getTile(x,y).equals(" * ") == true){
            game.generateMines(1);
            game.field[x][y] = " ? ";
        }

        game.clear(x,y);
        game.detect();
        game.update();


        while(true){
            if(game.getDone() == true && game.getWin() == true){
                System.out.println("You win!");
                game.onEnd();
                break;
            }else if(game.getDone() == true){
                game.onEnd();
                break;
            }else if(game.getDone() == false){
                x = -1;
                y = -1;
                System.out.print("X-Koordinate Eingeben:");
                y = scan.nextInt();
                System.out.print("Y-Koordinate Eingeben:");
                x = scan.nextInt();
                game.turn(x,y);
                game.isVictory();
                game.detect();
                game.update();
            }

        }
    }
}
