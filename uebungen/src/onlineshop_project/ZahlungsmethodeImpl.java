package onlineshop_project;

public abstract class ZahlungsmethodeImpl implements Zahlungsmethode{
    protected double zusatzkosten;

    public ZahlungsmethodeImpl(double zusatzkosten) {
        this.zusatzkosten = zusatzkosten;
    }

    public double berechneZusatzkosten(double gesamtpreis) {
        return gesamtpreis * zusatzkosten;
    }
}
