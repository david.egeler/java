package Students;

import java.util.HashMap;

public class Schule {
    private HashMap<String, Modul> modulers = new HashMap<>();

    public Modul getModul(String name) {
        return this.modulers.get(name);
    }
    public void removeModul(String name) {
        this.modulers.remove(name);
    }
    public void addModul(Modul modul){
        this.modulers.put(modul.name, modul);
    }


}
