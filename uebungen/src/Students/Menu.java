package Students;

import java.util.HashMap;
import java.util.Scanner;

public class Menu {
    int auswahl1;
    String nameModul;
    String nameSus;

    private String inNameDelete;

    Schule schule = new Schule();
    Scanner scanner = new Scanner(System.in);
    public void run(){
        while(auswahl1 != 6) {
            System.out.println("Schüler zu Kurs hinzufüegen: 1  Schüler entfernen: 2    " +
                    "Schülerliste zeigen: 3  Neues Modul erfassen: 4");
            int auswahl1 = scanner.nextInt();
            switch (auswahl1) {
                case 1:
                    System.out.println("Modulname:");
                    this.scanner.nextLine();
                    this.nameModul = this.scanner.nextLine();
                    System.out.println("Schülername:");
                    this.nameSus = this.scanner.nextLine();
                    if(this.schule.getModul(nameModul).addStudent(new Student(nameSus)) == true){
                        System.out.println("Adden Okey");
                    } else {
                        System.out.println("Voll");
                    }
                    break;
                case 2:
                    System.out.println("Geben sie den Namen des Schülers ein");
                    this.scanner.nextLine();
                    this.inNameDelete = scanner.nextLine();
                    System.out.println("Modulname eingeben:");
                    this.nameModul = this.scanner.nextLine();
                    this.schule.getModul(nameModul).removeStudent(inNameDelete);
                    break;
                case 3:
                    System.out.println("Modulname:");
                    this.scanner.nextLine();
                    this.nameModul = this.scanner.nextLine();
                    this.schule.getModul(nameModul).printAllSchüler();
                    break;
                case 4:
                    System.out.println("Modulname eingeben");
                    this.scanner.nextLine();
                    this.nameModul = this.scanner.nextLine();
                    System.out.println("Limit:");
                    int limit = this.scanner.nextInt();
                    System.out.print("Geben Sie alle Namen ein: (Done beendet eingaben)");
                    HashMap<String, Student> students2add = this.generateListOfStudents();
                    Modul modul2add = new Modul(nameModul, limit);
                    modul2add.setHashMapSchueler(students2add);
                    this.schule.addModul(modul2add);
                    this.schule.getModul(nameModul).printAllSchüler();

                    break;
            }
        }
    }

    public HashMap generateListOfStudents(){
        HashMap<String, Student> mapSchueler = new HashMap<>();
        boolean loop = true;
        while (loop){
            String name = scanner.nextLine();
            if (name.equals("Done")){
                return mapSchueler;
            }
            mapSchueler.put(name, new Student(name));
        }
        return mapSchueler;
    }
}
