package Students;

import java.util.HashMap;

public class Modul {
    String name;
    int limit;

    public Modul(String name, int limit) {
        this.name = name;
        this.limit = limit;
    }
    private HashMap<String, Student> schueler = new HashMap<>();

    public Student getStudent(String name) {
        return this.schueler.get(name);
    }

    public void removeStudent(String name) {
        this.schueler.remove(name);
    }
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    public boolean addStudent(Student schueler) {
        if(this.schueler.size() >= this.limit){
            return false;
        }
        this.schueler.put(schueler.name, schueler);
        return true;
    }

    public void printAllSchüler(){
        for(Student value : this.schueler.values()) {
            System.out.println(value.name);
        }
    }
    public void setHashMapSchueler(HashMap students){
        this.schueler = students;
    }
}
