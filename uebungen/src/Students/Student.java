package Students;

public class Student {
    String name;


    public Student(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}

