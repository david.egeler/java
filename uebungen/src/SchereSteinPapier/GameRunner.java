package SchereSteinPapier;

import java.util.Scanner;

public class GameRunner {
    Scanner scanner = new Scanner(System.in);
    int auswahl;
    int ziel;
    int scoreP1;
    int scoreP2;
    String moveP1;
    String moveP2;
    public void chooseGame(){
        System.out.println("Einzelspiel: 2       Mehrspieler: 1");
        this.auswahl = scanner.nextInt();
        if (this.auswahl == 1){
            einzelspieler();
        }else{

        }
    }
    public void einzelspieler(){
        System.out.println("Auf wieviel wollen sie Spielen?");
        this.ziel = scanner.nextInt();
        while (scoreP1 != this.ziel || scoreP2 != this.ziel){
            System.out.println("Spieler 1:");
            this.moveP1 = scanner.nextLine();
            System.out.println("Spieler 2:");
            this.moveP2 = scanner.nextLine();
                if(moveP1 == "schere" && moveP2 == "papier"){
                    scoreP1 ++;
                } else if (moveP1 == "stein" && moveP2 == "schere") {
                    scoreP1 ++;
                } else if (moveP1 == "papier" && moveP2 == "stein"){
                    scoreP1 ++;
                }else{
                    scoreP2 ++;
                }
        }
        if (scoreP1 > scoreP2){
            System.out.println("Spieler 1 hat gewonnen");
        } else {
            System.out.println("Spieler 2 hat gewonnen");
        }
    }
}
