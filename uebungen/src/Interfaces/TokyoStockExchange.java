package Interfaces;

import java.util.HashMap;
import java.util.Map;

class TokyoStockExchange implements StockExchange {
    private Map<String, Integer> stocks;
    private double balance;

    public TokyoStockExchange() {
        stocks = new HashMap<>();
        balance = 10000.0;
    }

    public double getCurrentStockPrice(String symbol) {
        return 100.0;
    }

    public void buyStock(String symbol, int amount) {
        double price = getCurrentStockPrice(symbol);
        double cost = price * amount;
        if (cost > balance) {
            throw new RuntimeException("Insufficient funds");
        }
        int currentAmount = stocks.getOrDefault(symbol, 0);
        stocks.put(symbol, currentAmount + amount);
        balance -= cost;
        System.out.println("Bought " + amount + " shares of " + symbol + " for " + cost + " on Tokyo Stock Exchange");
    }

    public void sellStock(String symbol, int amount) {
        double price = getCurrentStockPrice(symbol);
        int currentAmount = stocks.getOrDefault(symbol, 0);
        if (currentAmount < amount) {
            throw new RuntimeException("Insufficient stocks");
        }
        stocks.put(symbol, currentAmount - amount);
        double revenue = price * amount;
        balance += revenue;
        System.out.println("Sold " + amount + " shares of " + symbol + " for " + revenue + " on Tokyo Stock Exchange");
    }
}
