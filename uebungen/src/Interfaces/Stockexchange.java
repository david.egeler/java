package Interfaces;
interface StockExchange {
    double getCurrentStockPrice(String symbol);

    void buyStock(String symbol, int amount);

    void sellStock(String symbol, int amount);
}









