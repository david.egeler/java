package Interfaces;

class Portfolio {
    private StockExchange stockExchange;

    public Portfolio(StockExchange stockExchange) {
        this.stockExchange = stockExchange;
    }

    public double calculateStockValue(String symbol, int shares) {
        double price = stockExchange.getCurrentStockPrice(symbol);
        return price * shares;
    }

    public void buyStock(String symbol, int amount) {
        stockExchange.buyStock(symbol, amount);
    }

    public void sellStock(String symbol, int amount) {
        stockExchange.sellStock(symbol, amount);
    }
}
