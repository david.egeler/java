package Interfaces;

public class Main {
    public static void main(String[] args) {
        StockExchange tokyoStockExchange = new TokyoStockExchange();
        Portfolio portfolio1 = new Portfolio(tokyoStockExchange);
        portfolio1.buyStock("TKY", 5);
        portfolio1.sellStock("TKY", 2);
        double value1 = portfolio1.calculateStockValue("TKY", 3);
        System.out.println("Portfolio Wert mit Tokyo Stock Exchange: " + value1);

        StockExchange nyStockExchange = new NYStockExchange();
        Portfolio portfolio2 = new Portfolio(nyStockExchange);
        portfolio2.buyStock("NY", 3);
        portfolio2.sellStock("NY", 1);
        double value2 = portfolio2.calculateStockValue("NY", 2);
        System.out.println("Portfolio Wert mit NY Stock Exchange: " + value2);
    }
}

