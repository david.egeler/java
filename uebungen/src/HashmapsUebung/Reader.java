package HashmapsUebung;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Reader {
    Scanner scanner = new Scanner(System.in);
    String input;
    HashMap<String,String> airport = new HashMap<String, String>();
    public void read() throws IOException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader("airports.csv"));
        String line =  null;
            while ((line = br.readLine()) != null) {
                String[] str = line.split(",");
                    airport.put(str[0], str[1]);
            }
            System.out.println("Flughafen ansehen: A -- Flughafen entfernen: E");
            input = scanner.nextLine();
            if(input.equalsIgnoreCase("a")) {
                getAirport();
            }else{
                deleteItem();
            }
        }

    public void getAirport(){
        System.out.println("Bitte Flughafen kürzel eingeben:");
        this.input = scanner.nextLine();
        System.out.println("Der Flughafen heisst: " + airport.get(input));
    }
    public void deleteItem(){
        System.out.println("Welchen Flughafen möchten sie entfernen");
        this.input = scanner.nextLine();
        airport.remove(input);
        System.out.println("Deinstallation erfolgreich");
    }

}
