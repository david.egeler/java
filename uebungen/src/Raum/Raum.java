package Raum;

public class Raum {
    public int Raum;
    public double länge;
    public double breite;
    public double höhe;
    public int stühle;
    public int tische;
    public int steckdosen;
    public int fenster;
    public double gebäudeNummer;
    public double zimmerNummer;
    public int personenKapazität;
}
