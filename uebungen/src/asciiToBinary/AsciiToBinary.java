package asciiToBinary;

import java.util.Scanner;

public class AsciiToBinary {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {

        getInput();
    }

    private static void getInput()
    {
        String repChar;
        String inputString;

        log("Bitte einen Buchstaben eingeben");
        repChar = input.next();

        if(repChar.length() < 1 | repChar.length() > 1)
        {
            logln("Bitte nur einen Buchstaben eingeben");
            getInput();
        }

        log("Bitte String eingeben");
        inputString = input.next();

        convertToBinary(repChar, inputString);
    }

    private static void convertToBinary(String repChar, String inputString)
    {
        String outputString = "";

        for(char c : inputString.toCharArray())
        {
            if(c != ' ')
            {
                outputString = outputString + " " +  Integer.toBinaryString((int) c);
            }
            else
            {
                outputString = outputString + " " + repChar;
            }
        }

        outputString = outputString.trim();
        outputBinary(outputString);
    }

    private static void outputBinary(String outputString)
    {
        log(outputString);
    }

    public static void log(Object aObject)
    {
        System.out.print(aObject);
    }

    public static void logln(Object aObject)
    {
        System.out.println(aObject);
    }

}
