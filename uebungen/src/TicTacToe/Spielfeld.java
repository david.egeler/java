package TicTacToe;

public class Spielfeld {
    private char feld[][];
    private int feldGroesse = 3;
    private char spieler1;
    private char spieler2;
    private int count;
    public final static int spieler1Win = 1;
    public final static int spieler2Win = 2;
    public final static int unentschieden = 3;
    public final static int nichtKomplett = 4;
    public final static int nichtGültig = 5;

    public Spielfeld(char spieler1, char spieler2) {
        feld = new char[feldGroesse][feldGroesse];
        for (int i = 0; i < feldGroesse; i++) {
            for (int j = 0; j < feldGroesse; j++) {
                feld[i][j] = ' ';
            }
        }
        this.spieler1 = spieler1;
        this.spieler2 = spieler2;

    }

    public int move(char symbol, int x, int y) {
        if (x < 0 || x >= feldGroesse || y < 0 || y >= feldGroesse || feld[x][y] != ' ') {
            return nichtGültig;
        }
        feld[x][y] = symbol;
        count++;

        if (feld[x][0] == feld[x][1] && feld[x][0] == feld[x][2]) {
            return symbol == spieler1 ? spieler1Win : spieler2Win;
        }
        if (feld[0][y] == feld[1][y] && feld[0][y] == feld[2][y]) {
            return symbol == spieler1 ? spieler1Win : spieler2Win;
        }
        if (feld[0][0] != ' ' && feld[0][0] == feld[1][1] && feld[0][0] == feld[2][2]) {
            return symbol == spieler1 ? spieler1Win : spieler2Win;
        }
        if (feld[0][2] != ' ' && feld[0][2] == feld[1][1] && feld[0][2] == feld[2][0]) {
            return symbol == spieler1 ? spieler1Win : spieler2Win;
        }
        if (count == feldGroesse * feldGroesse) {
            return unentschieden;
        }
        return nichtKomplett;
    }
    public void feldZeigen(){
    for (int i = 0; i < feldGroesse; i++){
        for (int j = 0; j < feldGroesse; j++){
            System.out.print(feld[i][j]);
        }

        System.out.println();

    }
    }

}

