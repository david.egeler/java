package TicTacToe;

import java.util.Scanner;

public class Main {
    private Spieler spieler1, spieler2;
    private Spielfeld spielfeld;

    public static void main(String[] args) {
      Main tictactoe = new Main();
      tictactoe.spielStart();
    }
    public void spielStart(){
        Scanner scanner = new Scanner(System.in);
        spieler1 = takeSpielerInput(1);
        spieler2 = takeSpielerInput(2);
            while(spieler1.getSymbol() == spieler2.getSymbol()){
                System.out.println("Symbol bereits vergeben wähle ein anderes.");
                char symbol = scanner.next().charAt(0);
                spieler2.setSymbol(symbol);
            }
            Spielfeld feld = new Spielfeld(spieler1.getSymbol(),spieler2.getSymbol());
            boolean sp1Zug = true;
            int status = Spielfeld.nichtKomplett;
            while (status == Spielfeld.nichtKomplett || status == Spielfeld.nichtGültig){
                if (sp1Zug) {
                    System.out.println("Zug von:" + spieler1.getName());
                    System.out.println("X-koordinate eingeben:");
                    int x = scanner.nextInt();
                    System.out.println("Y-Koordinate eingeben:");
                    int y = scanner.nextInt();
                    status = feld.move(spieler1.getSymbol(), x, y);
                    if (status != Spielfeld.nichtGültig) {
                        sp1Zug = false;
                        feld.feldZeigen();
                    } else {
                        System.out.println("Das geht NIIIIICHT!!!!");
                    }
                }else{
                    System.out.println("Zug von:" + spieler2.getName());
                    System.out.println("X-koordinate eingeben:");
                    int x = scanner.nextInt();
                    System.out.println("Y-Koordinate eingeben:");
                    int y = scanner.nextInt();
                        status = feld.move(spieler2.getSymbol(), x, y);
                        if (status!= Spielfeld.nichtGültig) {
                            sp1Zug = true;
                            feld.feldZeigen();
                        }else{
                            System.out.println("Das geht NIIIIICHT!!!!");
                        }
                    }
                }
            if (status == Spielfeld.spieler1Win){
                System.out.println(spieler1.getName()+" hat gewonnennn");
            }else if(status == Spielfeld.spieler2Win){
                System.out.println(spieler2.getName()+" hat gewonnennn");
            }else{
                System.out.println("Unentschieden");

            }
            }
            private Spieler takeSpielerInput(int num) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Namen von Spieler " + num + " eingeben");
                String name = scanner.nextLine();
                System.out.println("Bitte Symbol für Spieler " + num + "eingeben");
                char symbol = scanner.next().charAt(0);
                Spieler s = new Spieler(name, symbol);
                return s;
            }
}
