package minmax;

import minmax.Berechnen;

public class MinMax {
    public static void main(String[] args) {
        Berechnen aufgabe = new Berechnen(new int[]{1, 2, 3, 4, 5});
        Berechnen gugus = new Berechnen(new int[]{5, -19, 55, 466, 4});

        aufgabe.minmaxholen();
        gugus.minmaxholen();
    }

}
