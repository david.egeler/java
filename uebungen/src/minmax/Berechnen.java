package minmax;

public class Berechnen {
    private int[] zahlen;

    public Berechnen(int[] zahlen) {
        this.zahlen = zahlen;
    }

    public void minmaxholen() {
        int min = zahlen[0];
        int max = zahlen[0];
        for (int i : zahlen){
            if(i<min){
                min = i;
            }else if (i>max){
                max = i;
            }
        }
        System.out.println("Die kleinste Zahl ist: " + min);
        System.out.println("Die grösste Zahl ist: " + max);
    }
}
