package blackjack;

public class Karte {
    private Zeichen zeichen;
    private Value value;

    public Karte(Zeichen zeichen, Value value) {
        this.zeichen = zeichen;
        this.value = value;
    }

    public String toString() {
        return this.zeichen.toString() + " " + this.value.toString();
    }

    public Value getValue() {
        return value;
    }

}
