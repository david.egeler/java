package blackjack;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private ArrayList<Karte> karten;

    public Deck() {
        this.karten = new ArrayList<Karte>();
    }

    public void createFullDeck(){
        // Karten generieren
        // bei Zeichen.values referenziert Zeichen auf das enum
        for(Zeichen karteZeichen : Zeichen.values()){
            for(Value karteValue : Value.values()){
                // Karte ins Deck (arraylist) fügen mithilfe vom Konstruktor in Klasse Karte
                this.karten.add(new Karte(karteZeichen, karteValue));
            }
        }
    }
    public String toString(){
        String kartenListeOutput = "";
        int i = 0;
        for (Karte aKarte : this.karten){
            kartenListeOutput += "\n" + aKarte.toString();
            i++;
        }
        return kartenListeOutput;
    }
    public void mischen(){
        ArrayList<Karte> tmpDeck = new ArrayList<Karte>();
        Random random = new Random();
        int randomKartenInd = 0;
        // gibt die grösse des Kartendecks
        int originalSize = this.karten.size();
        for (int i = 0; i < originalSize; i++){
            //Zufälliger Index generieren
            randomKartenInd = random.nextInt((this.karten.size()-1 - 0) + 1) + 0;
            tmpDeck.add(this.karten.get(randomKartenInd));
            // Karte vom Originalen Deck löschen
            this.karten.remove(randomKartenInd);
        }
        //Hier sind nun die Gemischten Karten
        this.karten = tmpDeck;
    }
    public void removeKarte(int i){
        this.karten.remove(i);
    }
    public void addKarte(Karte addKarte){
        this.karten.add(addKarte);
    }
    public Karte getKarte(int i){

        return this.karten.get(i);
    }
    public void draw(Deck comingFrom){
        this.karten.add(comingFrom.getKarte(0));
        comingFrom.removeKarte(0);
    }

    public int deckSize(){
        return this.karten.size();
    }

    public void moveAllToDeck(Deck moveTo){
        int thisDeckSize = this.karten.size();
        // Karten --> moveTo Deck
        for(int i = 0; i < thisDeckSize; i++){
            moveTo.addKarte(this.getKarte(i));
        }
        for (int i = 0; i < thisDeckSize; i++){
            this.removeKarte(0);
        }
    }

    // Gibt wert der Karten im Deck zurück
    public int cardsValue() {
        int totalValue = 0;
        int ass = 0;
        for (Karte aKarte : this.karten) {
            switch (aKarte.getValue()) {
                case zwei:
                    totalValue += 2;
                    break;
                case drei:
                    totalValue += 3;
                    break;
                case vier:
                    totalValue += 4;
                    break;
                case fuenf:
                    totalValue += 5;
                    break;
                case sechs:
                    totalValue += 6;
                    break;
                case sieben:
                    totalValue += 7;
                    break;
                case acht:
                    totalValue += 8;
                    break;
                case neun:
                    totalValue += 9;
                    break;
                case zehn:
                    totalValue += 10;
                    break;
                case bube:
                    totalValue += 10;
                    break;
                case dame:
                    totalValue += 10;
                    break;
                case koenig:
                    totalValue += 10;
                    break;
                case ass:
                    totalValue += 11;
            }
        }
        for (int i = 0; i < ass; i++) {


            if (totalValue > 10) {

                totalValue += 1;

            } else {
                totalValue += 11;
            }

        }
        return totalValue;
    }

}
