package blackjack;

import java.util.Scanner;

public class BlackJack {
    public void run() {

        System.out.println("Willkommen beim Gamblen");

        Deck spielDeck = new Deck();
        spielDeck.createFullDeck();
        spielDeck.mischen();

        Deck spielerDeck = new Deck();
        Deck dealerDeck = new Deck();

        double spielerGeld = 100.00;
        Scanner userInput = new Scanner(System.in);

        while(spielerGeld > 0){
            System.out.println("Sie haben " + spielerGeld + "CHF Guthaben, wie viel möchten sie setzen");
            double spielerEinsatzt = userInput.nextDouble();
            if (spielerEinsatzt > spielerGeld){
                System.out.println("Sie sind zu arm!");
                break;
            }

            boolean rundenEnde = false;
            // Karten ausgeben
            spielerDeck.draw(spielDeck);
            spielerDeck.draw(spielDeck);
            dealerDeck.draw(spielDeck);
            dealerDeck.draw(spielDeck);

            while(true){
                System.out.println("Ihre Karten: ");
                System.out.println(spielerDeck.toString());
                System.out.println("Sie haben eine Summe von: " + spielerDeck.cardsValue());

                // Dealer Karten zeigen
                System.out.println("Dealer Karten: " + dealerDeck.getKarte(0).toString() + " [?]");
                System.out.println("Karte ziehen[1]    Bleiben[2]");
                int auswahl = userInput.nextInt();

                //Karte ziehen
                if(auswahl == 1){
                    spielerDeck.draw(spielDeck);
                    System.out.println("Ihre Karte: " + spielerDeck.getKarte(spielerDeck.deckSize()-1).toString());
                    if (spielerDeck.cardsValue() > 21){
                        System.out.println("Zu viel. Ihre Kartensumme beträgt: " + spielerDeck.cardsValue());
                        spielerGeld -= spielerEinsatzt;
                        rundenEnde = true;
                        break;
                    }
                }
                if(auswahl == 2){
                    break;
                }
                else{
                    System.out.println("Ungültige Eingabe");
                }
            }
            // Dealer Karten zeigen
            System.out.println("Dealer Karten: " + dealerDeck.toString());
            if ((dealerDeck.cardsValue() > spielerDeck.cardsValue()) && rundenEnde == false){
                System.out.println("Sie haben verloren :( ");
                spielerGeld -= spielerEinsatzt;
                rundenEnde = true;
            }else if(spielerDeck.cardsValue() == 21){
                System.out.println("Sie haben ein Blackjack Super!!");
                spielerGeld += spielerEinsatzt * 1.5;
                rundenEnde = true;
            }
            while ((dealerDeck.cardsValue() < 17) && rundenEnde == false){
                dealerDeck.draw(spielDeck);
                System.out.println("Dealer zieht: " + dealerDeck.getKarte(dealerDeck.deckSize()-1).toString());
            }
            // Dealer Total ausgeben
            System.out.println("Dealer hat eine Summe von: " + dealerDeck.cardsValue());
            // Dealer verloren?
            if((dealerDeck.cardsValue() > 21) && rundenEnde == false){
                System.out.println("Dealer hat überzogen! Sie gewinnen!");
                spielerGeld += spielerEinsatzt;
            }
            // unentschieden
            else if ((spielerDeck.cardsValue() == dealerDeck.cardsValue()) && rundenEnde == false){
                System.out.println("Unentschieden");
                rundenEnde = true;
            }
            else if ((spielerDeck.cardsValue() > dealerDeck.cardsValue()) && rundenEnde == false){
                System.out.println(" Sie haben gewonnen!");
                spielerGeld += spielerEinsatzt;
                rundenEnde = true;
            }
            else if ((dealerDeck.cardsValue() > spielerDeck.cardsValue()) && rundenEnde == false){
                System.out.println("Sie haben verloren!");
                spielerGeld -= spielerEinsatzt;
                rundenEnde = true;
            }
            spielerDeck.moveAllToDeck(spielDeck);
            dealerDeck.moveAllToDeck(spielDeck);
            System.out.println("Neue Runde");
        }
        System.out.println("Game over sie haben kein Geld mehr");
    }
}
