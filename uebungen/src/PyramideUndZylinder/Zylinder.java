package PyramideUndZylinder;

import java.util.Scanner;

public class Zylinder {
   double radius;
   double hoehe;
   double volumen;
   double oberflaeche;
   Scanner scanner = new Scanner(System.in);
   String auswahl;

   public void getInput(){
      System.out.println("Hoehe: ");
      this.hoehe = scanner.nextDouble();
      System.out.println("Radius: ");
      this.radius = scanner.nextDouble();
      System.out.println("Für das Volumen: v,  für die Oberflaeche: o");
      this.scanner.nextLine();
      this.auswahl = scanner.nextLine();
      if (this.auswahl == "v" || this.auswahl == "V") {
            calcVolumen();
      } else {
            calcOberflaeche();
      };
   }
   public void calcVolumen(){
      volumen = Math.PI * (radius * radius) * hoehe;
      System.out.println("Volumen: " + volumen);
   }
   public void calcOberflaeche(){
      oberflaeche = (2 * Math.PI * radius * radius) + (2 * Math.PI * radius * hoehe);
      System.out.println("Oberflaeche: " + oberflaeche);
   }
}
