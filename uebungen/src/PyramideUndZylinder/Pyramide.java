package PyramideUndZylinder;

import java.util.Scanner;

public class Pyramide {
    double hoehe;
    double laenge;
    double breite;
    double volumen;
    double grundflaeche;
    String auswahl;
    Scanner scanner = new Scanner(System.in);

    public void getInput(){
        System.out.println("Hoehe: ");
        this.hoehe = scanner.nextDouble();
        System.out.println("Breite: ");
        this.breite = scanner.nextDouble();
        System.out.println("Länge: ");
        this.laenge = scanner.nextDouble();
        System.out.println("Für das Volumen: v,  für die Grundflaeche: g");
        this.scanner.nextLine();
        this.auswahl = scanner.nextLine();
        if(this.auswahl == "v" || this.auswahl == "V"){
            getVolumen();
        }else {
            getGrundflaeche();
        }
    }
    public void getVolumen(){
        volumen = (1/3)*(laenge * breite)*hoehe;
        System.out.println("Volumen" + volumen);
    }
    public void getGrundflaeche(){
        grundflaeche = laenge * breite;
        System.out.println("Grundfläche" + grundflaeche);
    }
}


